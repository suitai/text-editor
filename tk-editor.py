#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os.path
import time
import Tkinter
import tkFileDialog
import ScrolledText

# 名前ラベル
class name_label():
    def __init__(self, parent, name = ''):
        self.name = Tkinter.StringVar()
        self.name.set(name)
        self.label = Tkinter.Label(parent, textvariable = self.name)
    def set(self, name = ''):
        self.name.set(name)
    def pack(self, side = 'left'):
        self.label.pack(side = side)
# 時計ラベル
class ticker_label():
    def __init__(self, parent):
        self.label = name_label(parent)
        self.parent = parent
    def pack(self, side = 'right'):
        self.label.pack(side = side)
        self.show_time()
    def show_time(self):
        self.label.set(time.strftime('%I:%M:%S'))
        self.parent.after(1000, self.show_time)
# メニューバー
class menu_bar():
    def __init__(self, parent):
        self.menu = Tkinter.Menu(parent)
        parent.configure(menu = self.menu)
        self.m_file = Tkinter.Menu(self.menu, tearoff = 0)
    def set_file(self, open, close):
        self.m_file.add_command(label = 'Open', under = 0, command = open)
        self.m_file.add_separator
        self.m_file.add_command(label = 'Close', under = 0, command = close)
        self.menu.add_cascade(label = 'File', under = 0, menu = self.m_file)
# テキスト操作
class text_manager():
    def __init__(self, parent):
        self.parent = parent
        self.text = ScrolledText.ScrolledText(parent)
    def pack(self):
        self.text.pack()
    def show_file(self):
        self.file = open(self.filename)
        self.text.delete('1.0', 'end')
        for word in self.file:
            try:
                self.text.insert('end', word.decode('utf-8'))
            except:
                self.text.insert('end', word.decode('shift-jis'))
        self.file.close()
        self.text.focus_set()
    def load_file(self):
        self.path = ''
        self.filename = tkFileDialog.askopenfilename(filetypes = [('Text Files', '.txt')],
                                                     initialdir = self.path)
        if self.filename != '':
            self.path = os.path.dirname(self.filename)
            self.show_file()
            filename = self.filename.replace(self.path + '/', '')
            self.at_open(filename)

# main
if __name__ == "__main__":
    root_tk = Tkinter.Tk()
    root_tk.option_add('*font', ('FixedSys', 14))
    root_tk.title('Text Viewer')

    menu   = menu_bar(root_tk)
    text   = text_manager(root_tk)
    name   = name_label(root_tk)
    ticker = ticker_label(root_tk)

    menu.set_file(open = text.load_file, close = sys.exit)
    text.pack()
    name.pack(side = 'left')
    ticker.pack(side = 'right')
    text.at_open = name.set

    root_tk.mainloop()
